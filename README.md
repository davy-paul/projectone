# ProjectOne

This project demonstrates an expense reimbursement system where an employee can login, submit a reimbursement and view past pending and denied reimbursements. Then a manager can log it, look at all of the reimbursements and approve or deny the pending ones. It's deployed using a Tomcat server.
